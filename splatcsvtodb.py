#splatcsvtodb.py
"""
Simple command line script to read main and species tables from
the Splatalogue database and place the CSV files into a SQLite database

Usage - python splatcsvtodb.py splatalogue.db

Brian Kent, NRAO, 2012, for Tony Remijan
"""

__version__ = '0.1'
__all__ = [
    'splatCSVtoDB'
]

import math
import sys
import os
import string
import csv
import sqlite3
import argparse
import sys


class CommandLine:
    """Parse out command line options and parameters"""

    def __init__(self, sys):
        self.parser = argparse.ArgumentParser(prog='Splatalogue CSV to SQLlite',
            description='Convert Splatalogue CSV output to SQLlite',
            epilog='Learn more at http://www.splatalogue.net/')

        self.parser.add_argument('--version', action='version',
                                 version='%(prog)s 0.1.0')



def updateProgress(percent):
    """Update progress percentage - just a handy indicator"""
    sys.stdout.write("\r%d%%" %int(round(percent)))
    sys.stdout.flush()

    return None


def CSVtoSQL(dbfile):
    #Read in the header fields for table species
    fields = [k for k in open('species-fields.txt').read().split('\n') if k]
    csvfile = 'species.csv'
    reader = csv.DictReader(open(csvfile), fields, delimiter=':', quoting=csv.QUOTE_ALL)

    # Skip first line
    next(reader)

    conn = sqlite3.connect(dbfile)
    c = conn.cursor()
    try:
        c.execute('''DROP TABLE species''')
    except:
        c.execute('''CREATE TABLE species
             (species_id,name,chemical_name,s_name,s_name_noparens,SPLAT_ID,nlines,version,created,resolved,atmos,potential,probable,known_ast_molecules,planet,ism_hotcore,ism_diffusecloud,ism_darkcloud,comet,extragalactic,AGB_PPN_PN,SLiSE_IT,Top20)''')
             
    

    count = 0
    #Read in rows to SQLlite db
    for row in reader:
        #splat.append(row)
        sqlstring = "INSERT INTO species VALUES ("
        sqlstring += str(row['species_id']) + ", "
        sqlstring += "'"+str(row['name']) + "', "
        sqlstring += "'"+str(row['chemical_name']) + "', "
        sqlstring += "'"+str(row['s_name']) + "', "
        sqlstring += "'"+str(row['s_name_noparens']) + "', "
        sqlstring += "'"+str(row['SPLAT_ID']) + "', "
        sqlstring += str(row['nlines']) + ", "
        sqlstring += "'"+str(row['version']) + "', "
        sqlstring += "'"+str(row['created']) + "', "
        sqlstring += str(row['resolved']) + ", "
        sqlstring += str(row['atmos']) + ", "
        sqlstring += str(row['potential']) + ", "
        sqlstring += str(row['probable']) + ", "
        sqlstring += str(row['known_ast_molecules']) + ", "
        sqlstring += str(row['planet']) + ", "
        sqlstring += str(row['ism_hotcore']) + ", "
        sqlstring += str(row['ism_diffusecloud']) + ", "
        sqlstring += str(row['ism_darkcloud']) + ", "
        sqlstring += str(row['comet']) + ", "
        sqlstring += str(row['extragalactic']) + ", "
        sqlstring += str(row['AGB_PPN_PN']) + ", "
        sqlstring += "'"+str(row['SLiSE_IT']) + "', "
        sqlstring += str(row['Top20']) +  ") "
        
        #print >> sys.stderr, sqlstring
        #print >> sys.stderr, row.index
        #print >> sys.stderr, row
        c.execute(sqlstring)
    
        updateProgress(100.0*float(count)/float(1036))
        count = count + 1
    sys.stdout.write("\r...species table completed\n\n")
    sys.stdout.flush()
    
    conn.commit()
    #c.close()
    
    #######################################################################
    fields = [k for k in open('main-fields.txt').read().split('\n') if k]
    csvfile = 'main.csv'
    reader = csv.DictReader(open(csvfile), fields, delimiter=':', quoting=csv.QUOTE_ALL)

    # Skip first line
    next(reader)

    try:
        c.execute('''DROP TABLE main''')
    except:
        c.execute('''CREATE TABLE main
            (ll_id, intintensity, obsintensity_Lovas_NIST, sijmu2, aij, rel_int_HFS_Lovas, lower_state_energy, lower_state_energy_K, upper_state_energy, upper_state_energy_K, upper_state_degeneracy, resolved_QNs, Lovas_NRAO, transition_in_space, species_id, orderedfreq)''')
    
    count = 0
    #Read in rows to SQLlite db
    for row in reader:
        #splat.append(row)
        sqlstring = "INSERT INTO main VALUES ("
        sqlstring += str(row['ll_id']) + ", "
        sqlstring += "'"+str(row['intintensity']) + "', "
        sqlstring += "'"+str(row['obsintensity_Lovas_NIST']) + "', "
        sqlstring += str(row['sijmu2']) + ", "
        sqlstring += str(row['aij']) + ", "
        sqlstring += "'"+str(row['rel_int_HFS_Lovas']) + "', "
        sqlstring += str(row['lower_state_energy']) + ", "
        sqlstring += str(row['lower_state_energy_K']) + ", "
        sqlstring += str(row['upper_state_energy']) + ", "
        sqlstring += str(row['upper_state_energy_K']) + ", "
        sqlstring += str(row['upper_state_degeneracy']) + ", "
        sqlstring += "'"+str(row['resolved_QNs']) + "', "
        sqlstring += str(row['Lovas_NRAO']) + ", "
        sqlstring += str(row['transition_in_space']) + ", "
        sqlstring += str(row['species_id']) + ", "
        sqlstring += str(row['orderedfreq']) + ") "
        
        #print >> sys.stderr, sqlstring
        #print >> sys.stderr, row.index
        #print >> sys.stderr, row
        c.execute(sqlstring)
    
        updateProgress(100.0*float(count)/float(938256))
        count = count + 1
    sys.stdout.write("\r...main table completed.\n\n")
    sys.stdout.flush()
    
    #Add in indices for speed improvements
    conn.commit()
    c.execute('''CREATE INDEX IF NOT EXISTS speciesIndex ON main( species_id ASC);''')
    conn.commit()
    c.execute('''CREATE INDEX if NOT EXISTS chemNameIndex on species(chemical_name ASC);''')
        
    
    #Remove white space from intintensity modify to NUMERIC
    c.execute('''ALTER TABLE main RENAME TO tmpMain;''')
    conn.commit()
    #c.execute('''DROP TABLE main;''')
    #conn.commit()
    c.execute('''CREATE TABLE main( ll_id INTEGER, intintensity NUMERIC, obsintensity_Lovas_NIST NUMERIC, sijmu2 NUMERIC, aij NUMERIC,rel_int_HFS_Lovas NUMERIC, lower_state_energy NUMERIC, lower_state_energy_K NUMERIC, upper_state_energy NUMERIC, upper_state_energy_K NUMERIC, upper_state_degeneracy INTEGER, resolved_QNs TEXT, Lovas_NRAO INTEGER, transition_in_space INTEGER, species_id INTEGER, orderedfreq NUMERIC );''')
    conn.commit()
    c.close()
    
    #Need SQLite 3.3.7 or higher otherwise execute as system command
    """
    c.execute('''INSERT INTO main(ll_id,intintensity,obsintensity_Lovas_NIST,sijmu2,aij,rel_int_HFS_Lovas, lower_state_energy,lower_state_energy_K, upper_state_energy, upper_state_energy_K, upper_state_degeneracy, resolved_QNs, Lovas_NRAO, transition_in_space, species_id, orderedfreq) SELECT ll_id, trim(intintensity), obsintensity_Lovas_NIST,sijmu2,aij,rel_int_HFS_Lovas, lower_state_energy, lower_state_energy_K, upper_state_energy, upper_state_energy_K,upper_state_degeneracy,resolved_QNs,Lovas_NRAO,transition_in_space,species_id,orderedfreq FROM tmpMain;''')
    conn.commit()    
    """
    os.system('sqlite3 '+ dbfile + ' "INSERT INTO main(ll_id,intintensity,obsintensity_Lovas_NIST,sijmu2,aij,rel_int_HFS_Lovas, lower_state_energy,lower_state_energy_K, upper_state_energy, upper_state_energy_K, upper_state_degeneracy, resolved_QNs, Lovas_NRAO, transition_in_space, species_id, orderedfreq) SELECT ll_id, trim(intintensity), obsintensity_Lovas_NIST,sijmu2,aij,rel_int_HFS_Lovas, lower_state_energy, lower_state_energy_K, upper_state_energy, upper_state_energy_K,upper_state_degeneracy,resolved_QNs,Lovas_NRAO,transition_in_space,species_id,orderedfreq FROM tmpMain"; ')
    
    conn = sqlite3.connect(dbfile)
    c = conn.cursor()
    c.execute('''DROP TABLE tmpMain;''')
    conn.commit()
    
    
    #Close out the connection
    c.close()



if __name__ == '__main__':
    
    if (len(sys.argv) == 1 or len(sys.argv) > 2):
        msg = ('\n*** Usage - python splatcsvtodb.py splatalogue.db ***\n')
        sys.exit(msg)
    
    if len(sys.argv) == 2:
        dbfile = sys.argv[1]
    
    CSVtoSQL(dbfile)

    