#!/usr/bin/env python
#setup.py for distutils installation
#Install with "python setup.py install --prefix=<place in your PYTHONPATH>"
#Brian Kent, NRAO 2012

from distutils.core import setup, Command

class PyTest(Command):
    """Class using distutils.core.Command to run unittests
    """
    user_options = []
    def initialize_options(self):
        pass
    def finalize_options(self):
        pass
    def run(self):
        import sys,subprocess
        errno = subprocess.call([sys.executable, 'datasplatclass_test.py'])
        raise SystemExit(errno)

#setup method
setup(name='datasplatclass',
      version='1.0',
      description='Datasplat interface for Splatalogue',
      long_description='Python and CASA modules for using Splatalogue',
      license='GPL',
      platforms='Linux, Mac OSX 10.7',
      author='Brian Kent',
      author_email='bkent@nrao.edu',
      maintainer='Brian Kent',
      maintainer_email='bkent@nrao.edu',
      url='https://bitbucket.org/brkent/datasplat, http://www.splatalogue.net',
      py_modules=['datasplatclass', 'commandline', 'argparse'],
      cmdclass = {'test': PyTest},
     )