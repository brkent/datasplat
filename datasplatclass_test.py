#! /usr/bin/env python
# Author: Brian Kent, NRAO, March 2012, bkent@nrao.edu

import datasplatclass
import unittest
from xml.dom.minidom import parse, parseString, Document

class TestSequenceFunctions(unittest.TestCase):
    """Unit tests for methods in datasplatclass.py
    """
    
    def setUp(self):
        """Test data
        """
        
        self.speciesIDNumber = 77
        self.temperature = 100
        self.columnDensity = 1.0E+14
        self.lineWidth = 10
        self.antennaDiameter = 100
        self.sourceSize = 1
        self.partFuncCoeff = 2.3
        self.partFuncExp = 1.5
        self.minFreq = 70000.0
        self.maxFreq = 350000.0
        
        self.orderedFreq = 119271.331
        self.Sijmu2 = 5.7366
        self.upperStateEnergyK = 423.420211064663
        self.XMLInputFile = 'sampleinputs.xml'
        self.XMLInputString = ('<dataSplatInput><speciesIDNumber>77</speciesIDNumber><temperature>100</temperature><columnDensity>1.0E+14</columnDensity><lineWidth>10</lineWidth><antennaDiameter>100</antennaDiameter><sourceSize>1</sourceSize><partFuncCoeff>2.3</partFuncCoeff><partFuncExp>1.5</partFuncExp><minFreq>70000</minFreq><maxFreq>350000</maxFreq></dataSplatInput>')
        self.sampleInputDict = ({u'dataSplatInput':
            {u'partFuncExp': u'1.5', u'maxFreq': u'350000',
             u'lineWidth': u'10', u'temperature': u'100',
             u'columnDensity': u'1.0E+14', u'speciesIDNumber': u'77',
             u'antennaDiameter': u'100', u'partFuncCoeff': u'2.3',
             u'sourceSize': u'1', u'minFreq': u'70000'}})

    
    def test_XMLFiletoDict(self):
        """Testing XML file input to dictionary
        """
        
        testDict = datasplatclass.readConfig(filename=self.XMLInputFile,
                                        fileinput=True)
        self.assertEqual(self.sampleInputDict, testDict)
    
    def test_XMLStringtoDict(self):
        """Testing XML string to dictionary
        """
        testDict = datasplatclass.readConfig(socket=True,
                                        socketString=self.XMLInputString)
        self.assertEqual(self.sampleInputDict, testDict)
        
    def test_stringToNode(self):
        """Test parsing of XML Document node to dictionary
        """
        
        testFileToDom = parse(self.XMLInputFile)
        testStringToDom = parseString(self.XMLInputString)
        self.assertTrue(isinstance(testFileToDom, Document))
        self.assertTrue(isinstance(testStringToDom, Document))
        self.assertEqual(datasplatclass.nodeToDic(testFileToDom),
                         self.sampleInputDict)
        self.assertEqual(datasplatclass.nodeToDic(testStringToDom),
                         self.sampleInputDict)
    
    def test_computeValsSingleDish(self):
        """Testing the computation methods in class DataSplat
        For Single Dish
        """
        
        d = datasplatclass.DataSplat()
        d.splatSQLTable = ([{'Sijmu2': 16.978000000000002,
                             'orderedFreq': 79228.491999999998,
                             'upperStateEnergyK': 1162.2801675061501,
                             'speciesName': u'Ammonia',
                             'resolvedQNs':  u'     11( 4, 8)0s-  11( 3, 8)0a'}])
        d.computeVals(singleDish=True, interferometer=False)
        f = d.computedValues[0]['orderedFreq']
        self.assertEqual(f, 79.228492000000003)
        
        
    def test_computeValsInterferometer(self):
        """Testing the computation methods in class DataSplat
        For Single Dish
        """
        
        d = datasplatclass.DataSplat()
        d.splatSQLTable = ([{'Sijmu2': 16.978000000000002,
                             'orderedFreq': 79228.491999999998,
                             'upperStateEnergyK': 1162.2801675061501,
                             'speciesName': u'Ammonia',
                             'resolvedQNs':  u'     11( 4, 8)0s-  11( 3, 8)0a'}])
        d.computeVals(singleDish=False, interferometer=True)
        f = d.computedValues[0]['orderedFreq']
        self.assertEqual(f, 79.228492000000003)

    def test_SQLliteCompute(self):
        """Testing query to SQLlite database version of Splatalogue
        and performing singleDish and interferometer computation"""
        
        d = datasplatclass.DataSplat()
        d.splatSQLlitequery()
        d.computeVals(singleDish=True, interferometer=True)
        d.createXMLSticks()
        
        self.assertEqual(d.computedValues[0]['inten'], 4.6393668111072833e-09)
        

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSequenceFunctions)
    unittest.TextTestRunner(verbosity=2).run(suite)