#! /usr/bin/env python
# Author: Brian Kent, NRAO, March 2012
# For Anthony Remijan and the Splatalogue team
# http://www.splatalogue.net
"""DataSplat class and associated methods

This file is a sample client application which sends
and receives XML from the main datasplatclass.py file

Sample usage in one window:
    
    python datasplatclass.py --socket
    
and in another window:
    
    python socket_client.py
    
socket_client.py will then open sampleinputs.xml, send that the
datasplatclass.py listening on a port, which will then perform
the computations and return an XML file of "sticks" to the client.



"""
import socket
from xml.dom.minidom import parse, parseString, Document

class NotTextNodeError:
    pass


def getTextFromNode(node):
    """
    scans through all children of node and gathers the
    text. if node has non-text child-nodes, then
    NotTextNodeError is raised.
    """
    t = ""
    for n in node.childNodes:
        if n.nodeType == n.TEXT_NODE:
            t += n.nodeValue
        else:
            raise NotTextNodeError
    return t


def nodeToDic(node):
    """
    nodeToDic() scans through the children of node and makes a
    dictionary from the content.
    three cases are differentiated:
        - if the node contains no other nodes, it is a text-node
    and {nodeName:text} is merged into the dictionary.
        - if the node has the attribute "method" set to "true",
    then it's children will be appended to a list and this
    list is merged to the dictionary in the form: {nodeName:list}.
        - else, nodeToDic() will call itself recursively on
    the nodes children (merging {nodeName:nodeToDic()} to
    the dictionary).
    """
    dic = {}
    for n in node.childNodes:
        if n.nodeType != n.ELEMENT_NODE:
            continue
        if n.getAttribute("multiple") == "true":
            # node with multiple children:
            # put them in a list
            l = []
            for c in n.childNodes:
                if c.nodeType != n.ELEMENT_NODE:
                    continue
                l.append(nodeToDic(c))
                dic.update({n.nodeName: l})
            continue

        try:
            text = getTextFromNode(n)
        except NotTextNodeError:
            # 'normal' node
            dic.update({n.nodeName: nodeToDic(n)})
            continue

        # text node
        dic.update({n.nodeName: text})
        continue
    return dic


def readConfig(filename):
    dom = parse(filename)
    return nodeToDic(dom)

port = 8081
host = "localhost"
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("", 0))

#Open XML file
filename='sampleinputs.xml'
try:
    #dict = readConfig(filename)
    dom=parse(filename)
    msg = dom.toxml()
except IOError:
    sys.exit("Unable to open / File does not exist")

BUFSIZE = 1024
while msg:
    s.sendto(msg[:BUFSIZE], (host, port))
    msg = msg[BUFSIZE:]