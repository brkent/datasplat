"""
# Author: Brian Kent, NRAO, May 2012
# For Anthony Remijan and the Splatalogue team
# http://www.splatalogue.net

This supplementary method combined with datasplat.xml allows
for the datasplat class to be utilized in CASA

Load into CASA with:
os.system('buildmytasks')
execfile('mytasks.py')
inp datasplat

"""

from taskinit import *
import datasplatclass as ds

def datasplat(speciesIDNumber=None, temperature=None, 
    columnDensity=None, lineWidth=None, antennaDiameter=None,
    sourceSize=None, partFuncCoeff=None, 
    partFuncExp=None, minFreq=None, maxFreq=None, asciifileout=None, 
    xmlfileout=None, plotDisplay=None, showTerminal=None):
    """Takes multiple inputs as a CASA task, and returns
    a dictionary containing the frequency / intensity values.
    """

    #casalog.origin('datasplat')
    #casalog.post('Remember to '+myinput)

    d = ds.DataSplat()
    
    d.setInputs(speciesIDNumber=speciesIDNumber, 
        temperature=temperature, 
        columnDensity=columnDensity, 
        lineWidth=lineWidth, 
        antennaDiameter=antennaDiameter, 
        sourceSize=sourceSize, 
        partFuncCoeff=partFuncCoeff, 
        partFuncExp=partFuncExp, 
        minFreq=minFreq, 
        maxFreq=maxFreq)

    d.splatSQLlitequery()
    d.computeVals()
    d.createXMLSticks()
    
    if (asciifileout): 
        d.printResults(filename=asciifileout, fileout=True, stdout=False)
    if (xmlfileout): 
        d.writeXMLSticks(filename=xmlfileout, fileout=True, stdout=False)
    if (plotDisplay): 
        d.plotSticks()
    if (showTerminal): 
        d.printResults(fileout=False, stdout=True)
    
    
    freqs = [item['orderedFreq'] for item in d.computedValues]
    intens = [item['inten'] for item in d.computedValues]
    
    values_dict = {'freqs':freqs, 'intensity':intens}
    
    return values_dict