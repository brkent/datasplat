#! /usr/bin/env python
# Author: Brian Kent, NRAO, March 2012, bkent@nrao.edu
import argparse
import sys


class CommandLine:
    """Parse out command line options and parameters"""

    def __init__(self, sys):
        self.parser = argparse.ArgumentParser(prog='DataSplat',
            description='DataSplat server module for computing intensities ' +
            'from Splatalogue data',
            epilog='Learn more at http://www.splatalogue.net/')

        self.parser.add_argument('--version', action='version',
                                 version='%(prog)s 1.0')

        self.parser.add_argument('--socket', action='store_true',
            default=False,
            help='Run server to listen for XML in put on a socket')

        self.parser.add_argument('--host', action='store',
            help='Specify host on which to listen')

        self.parser.add_argument('--port', action='store', type=int,
            help='Specify port for socket to listen')

        self.parser.add_argument('--gui', action='store_true', default=False,
            help='Interact with DataSplat via a GUI')

        self.parser.add_argument('--stdoutxml', action='store_true',
            default=False,
            help='Print freq and intensities to screen/stdout as XML')

        self.parser.add_argument('--stdoutascii', action='store_true',
            default=False,
            help='Print freq and intensities to screen as ASCII')

        self.parser.add_argument('--inputxml', action='store',
            help='Use an XML formatted file for input parameters')

        self.parser.add_argument('--inputascii', action='store',
            help='Use an ASCII formatted file for input paraemters')

        self.parser.add_argument('--outputxml', action='store',
            help='Print freq and intensities to XML file')

        self.parser.add_argument('--outputascii', action='store',
            help='Print freq and intensities to ASCII file')

        self.parser.add_argument('--offline', action='store_true',
            help='Use offline version of Splatalogue')

        self.parser.add_argument('--demo', action='store_true',
            help='Run demo of DataSplat')

if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.argv.append('-h')
    c = CommandLine(sys)
    args = c.parser.parse_args()
