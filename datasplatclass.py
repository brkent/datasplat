#! /usr/bin/env python
# Author: Brian Kent, NRAO, March 2012
# For Anthony Remijan and the Splatalogue team
# http://www.splatalogue.net
"""DataSplat class and associated methods

This module take XML, ASCII, commandline, and class
parameter inputs, queries the spectral
line service splatalogue, computes intensities and frequencies,
and returns those values.

The class requires inputs of speciesIDNumber, temperature, columnDensity,
lineWidth, antennaDiameter,
sourceSize,partFuncCoeff,partFuncExp, minFreq, and maxFreq,
as well as the upper state energy in Kelvin, resolved
quantum numbers, and Sij (mu^2) to complete the calculation.

The class requires matplotlib and MySQLdb (if SQL is queried).
http://matplotlib.sourceforge.net/
http://sourceforge.net/projects/mysql-python/

The class requires argparse (included for use with Python 2.6)
http://pypi.python.org/pypi/argparse

Testing completed on Python 2.6 and 2.7.

A sample usage at a standard python prompt or in IPython or CASA:

#Example notes for CASA task (not completed yet):
/usr/lib64/casapy/stable/lib/python2.6/importfits.py
/usr/lib64/casapy/stable/lib/python2.6/importfits_cli.py

import datasplatclass as ds
d=ds.DataSplat()
d.splatSQLlitequery()
d.computeVals()
d.createXMLSticks()
d.writeXMLSticks(filename='test.xml', stdout=True, fileout=False)
d.printResults(filename='test.dat', stdout=True, fileout=False)
d.plotSticks()



"""

__version__ = '0.1'
__all__ = [
    'DataSplat'
]

import math
import sys
import os
from xml.dom.minidom import parse, parseString, Document
import string
import pylab
import socket
import commandline
import sqlite3


def interrupt(message=''):
    """Exit if interrupted
    """
    print >> sys.stderr, 'Interrupted ' + message
    sys.exit(2)


## {{{ http://code.activestate.com/recipes/116539/ (r1)
class NotTextNodeError:
    pass


def getTextFromNode(node):
    """
    scans through all children of node and gathers the
    text. if node has non-text child-nodes, then
    NotTextNodeError is raised.
    """
    t = ""
    for n in node.childNodes:
        if n.nodeType == n.TEXT_NODE:
            t += n.nodeValue
        else:
            raise NotTextNodeError
    return t


def nodeToDic(node):
    """
    nodeToDic() scans through the children of node and makes a
    dictionary from the content.
    three cases are differentiated:
        - if the node contains no other nodes, it is a text-node
    and {nodeName:text} is merged into the dictionary.
        - if the node has the attribute "method" set to "true",
    then it's children will be appended to a list and this
    list is merged to the dictionary in the form: {nodeName:list}.
        - else, nodeToDic() will call itself recursively on
    the nodes children (merging {nodeName:nodeToDic()} to
    the dictionary).
    """
    dic = {}
    for n in node.childNodes:
        if n.nodeType != n.ELEMENT_NODE:
            continue
        if n.getAttribute("multiple") == "true":
            # node with multiple children:
            # put them in a list
            l = []
            for c in n.childNodes:
                if c.nodeType != n.ELEMENT_NODE:
                    continue
                l.append(nodeToDic(c))
                dic.update({n.nodeName: l})
            continue

        try:
            text = getTextFromNode(n)
        except NotTextNodeError:
            # 'normal' node
            dic.update({n.nodeName: nodeToDic(n)})
            continue

        # text node
        dic.update({n.nodeName: text})
        continue
    return dic


def readConfig(filename='', fileinput=False, socket=False,
               socketString=''):
    """Parse file name or message XML input string from socket"""

    if (fileinput == True):
        try:
            dom = parse(filename)
        except IOError:
            sys.exit("Unable to open / File does not exist")
    if (socket == True):
        try:
            dom = parseString(socketString)
        except:
            sys.exit("Unable to parse incoming XML.")

    return nodeToDic(dom)


class DataSplat:
    """DataSplat class with methods for importing physical parameters,
    querying the MySQL Splatalogue database, and computing frequency/
    intensity parameters that can be displayed or exported in several
    formats.
    """

    def __init__(self, speciesIDNumber=None, temperature=None,
        columnDensity=None, lineWidth=None, antennaDiameter=None,
        sourceSize=None, partFuncCoeff=None,
        partFuncExp=None, minFreq=None, maxFreq=None,
        thetaAinter=None, thetaBinter=None, deltav=None):

        self.attr = {}

        #self.orderedFreq = 119271.331
        #self.Sijmu2 = 5.7366
        #self.upperStateEnergyK = 423.420211064663

        self.docXML = Document()

        self.splatSQLTable = []
        self.computedValues = []

        if speciesIDNumber is None:
            speciesIDNumber = 77
        self.speciesIDNumber = speciesIDNumber

        if temperature is None:
            temperature = 100.0
        self.temperature = temperature

        if columnDensity is None:
            columnDensity = 1.e14
        self.columnDensity = columnDensity

        if lineWidth is None:
            lineWidth = 10.0
        self.lineWidth = lineWidth

        if antennaDiameter is None:
            antennaDiameter = 100.0
        self.antennaDiameter = antennaDiameter

        if sourceSize is None:
            sourceSize = 1.0
        self.sourceSize = sourceSize

        if partFuncCoeff is None:
            partFuncCoeff = 2.3
        self.partFuncCoeff = partFuncCoeff

        if partFuncExp is None:
            partFuncExp = 1.5
        self.partFuncExp = partFuncExp

        if minFreq is None:
            minFreq = 77000.0
        self.minFreq = minFreq

        if maxFreq is None:
            maxFreq = 350000.0
        self.maxFreq = maxFreq
        
        if thetaAinter is None:
            thetaAinter = 2.68
        self.thetaAinter = thetaAinter
        
        if thetaBinter is None:
            thetaBinter = 2.13
        self.thetaBinter = thetaBinter
        
        if deltav is None:
            deltav = 5.0
        self.deltav = deltav

    def setInputs(self, speciesIDNumber=None, temperature=None,
        columnDensity=None, lineWidth=None, antennaDiameter=None,
        sourceSize=None, partFuncCoeff=None,
        partFuncExp=None, minFreq=None, maxFreq=None, thetaAinter=None,
        thetaBinter=None, deltav=None):

        if speciesIDNumber is not None:
            self.speciesIDNumber = speciesIDNumber

        if temperature is not None:
            self.temperature = temperature

        if columnDensity is not None:
            self.columnDensity = columnDensity

        if lineWidth is not None:
            self.lineWidth = lineWidth

        if antennaDiameter is not None:
            self.antennaDiameter = antennaDiameter

        if sourceSize is not None:
            self.sourceSize = sourceSize

        if partFuncCoeff is not None:
            self.partFuncCoeff = partFuncCoeff

        if partFuncExp is not None:
            self.partFuncExp = partFuncExp

        if minFreq is not None:
            self.minFreq = minFreq

        if maxFreq is not None:
            self.maxFreq = maxFreq
        
        if thetaAinter is not None:
            self.thetaAinter = thetaAinter
        
        if thetaBinter is not None:
            self.thetaBinter = thetaBinter
        
        if deltav is not None:
            self.deltav = deltav

    def splatSQLlitequery(self, dbfile='splatalogue.db', 
        sqldbcols=('orderedfreq',
            'upper_state_energy_K', 'resolved_QNs', 'sijmu2'),
        tableName='main',
        columns=('orderedFreq', 'upperStateEnergyK',
            'resolvedQNs', 'Sijmu2')):
        """Query the Splatalogue SQLlite database and return a dictionary"""
        try:
            conn = sqlite3.connect(dbfile)
            cursor = conn.cursor()
            
            speciesIDNumber = str(self.speciesIDNumber)
            minFreq = str(self.minFreq)
            maxFreq = str(self.maxFreq)
            
            queryString = ("select " + string.join(sqldbcols, ',') +
                " from " + tableName +
                " where species_id=" + speciesIDNumber +
                " and orderedfreq>" + minFreq +
                " and orderedfreq<" + maxFreq +
                " order by orderedfreq")

            print queryString

            cursor.execute(queryString)
            sqltuples = cursor.fetchall()
            
            table = []

            for row in sqltuples:
                newrow = [(row[i], columns[i]) for i in range(0, len(row))]
                table.append(dict((y, x) for x, y in newrow))

            cursor.close()
            conn.close()

            self.splatSQLTable = table

            return True
        except sqlite3.Error, e:
            print "An error occurred:", e.args[0]
            
        

    def splatSQLquery(self, host, user, passwd, db,
        sqldbcols=('species.name', 'main.orderedfreq',
            'main.upper_state_energy_K', 'main.resolved_QNs', 'main.Sijmu2'),
        tableName='main,species',
        columns=('speciesName', 'orderedFreq', 'upperStateEnergyK',
            'resolvedQNs', 'Sijmu2')):
        """Query the Splatalogue MySQL database and return a dictionary"""
        
        #See http://lucumr.pocoo.org/2011/9/21/python-import-blackbox/
        try:
            import MySQLdb
        except ImportError:
            exc_type, exc_value, tb_root = sys.exc_info()
            tb = tb_root
            while tb is not None:
                if tb.tb_frame.f_globals.get('__name__') == module_name:
                    raise exc_type, exc_value, tb_root
                tb = tb.tb_next
            return None

        try:
            conn = MySQLdb.connect(host=host,
                user=user, passwd=passwd, db=db)
            cursor = conn.cursor()

            speciesIDNumber = str(self.speciesIDNumber)
            minFreq = str(self.minFreq)
            maxFreq = str(self.maxFreq)

            queryString = ("select " + string.join(sqldbcols, ',') +
                " from " + tableName +
                " where main.Lovas_NRAO=1 and main.species_id=" +
                speciesIDNumber +
                " and species.species_id=" + speciesIDNumber +
                " and main.orderedfreq>" + minFreq +
                " and main.orderedfreq<" + maxFreq +
                " order by main.orderedfreq")

            cursor.execute(queryString)
            sqltuples = cursor.fetchall()

            table = []

            for row in sqltuples:
                newrow = [(row[i], columns[i]) for i in range(0, len(row))]
                table.append(dict((y, x) for x, y in newrow))

            cursor.close()
            conn.close()

            self.splatSQLTable = table

            return True

        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
            print "Entering demo values..."
            self.orderedFreq = 119271.331
            self.Sijmu2 = 5.7366
            self.upperStateEnergyK = 423.420211064663

    def computeVals(self, singleDish=True, interferometer=False):
        """compute values from the splatSQLTable list of dictionaries"""

        #Speed of light m/s
        c = 299792458.0
        computedVals = []
        
        if (singleDish == True):
            for row in self.splatSQLTable:
                const1 = row['orderedFreq'] / 1000.0
                const0 = row['upperStateEnergyK']
                arg1 = (
                    1.0 -
                    ((math.exp(0.000048 * row['orderedFreq'] /
                        self.temperature) - 1.0) /
                        (math.exp(0.000048 * row['orderedFreq'] / 2.78)
                    - 1.0)))
                thetaB = ((206265.0 * c) / (row['orderedFreq'] * 1.e6 *
                    self.antennaDiameter))
                srcSq = self.sourceSize ** 2
                fillingFactor = srcSq / (srcSq + thetaB ** 2)
                arg2 = ((self.columnDensity * fillingFactor *
                         row['orderedFreq'] * row['Sijmu2']) / 1.8e14)
                arg3 = math.exp(row['upperStateEnergyK'] / self.temperature)
                partFunc = (self.partFuncCoeff * (self.temperature **
                    self.partFuncExp))
                inten = (((arg1 * arg2) / (arg3 * partFunc)) /
                    (self.lineWidth * 1000.0))

                telescope = 'single dish'

                calculations = (('orderedFreq', const1),
                    ('upperStateEnergyK', const0),
                    ('thetaB', thetaB),
                    ('partFunc', partFunc),
                    ('inten', inten),
                    ('telescope', telescope))

                computedVals.append(
                    dict((key, val) for key, val in calculations))
        
        if (interferometer == True):
            for row in self.splatSQLTable:
                
                partFunc = (self.partFuncCoeff * (self.temperature **
                    self.partFuncExp))
                Q_rot = partFunc
                const1 = row['orderedFreq'] / 1000.0
                const0 = row['upperStateEnergyK']
                freq = row['orderedFreq']/1000.0 #Now in GHz
                Sijmu2 = row['Sijmu2']
                thetaA = self.thetaAinter
                thetaB = self.thetaBinter
                srcSq = self.sourceSize ** 2
                fillingFactor = srcSq / (srcSq + thetaB ** 2)
                B = fillingFactor
                arg3 = math.exp(row['upperStateEnergyK'] / self.temperature)
                N_Tot = self.columnDensity
                deltav = self.deltav # km/s

                #Frequency needs to be in GHz
                #See equation 5 of Hollis et al. 2003
                #http://adsabs.harvard.edu/abs/2003ApJ...588..353H
                inten = ((N_Tot * B * thetaA * thetaB * freq**3 * Sijmu2) /
                    (2.04 * 10**20 * deltav * Q_rot * arg3))
                
                telescope = 'interferometer'
                
                calculations = (('orderedFreq', const1),
                    ('upperStateEnergyK', const0),
                    ('thetaB', thetaB),
                    ('partFunc', partFunc),
                    ('inten', inten),
                    ('telescope', telescope))

                computedVals.append(
                    dict((key, val) for key, val in calculations))

        self.computedValues = computedVals

        return True

    def createXMLSticks(self, keyOrder=('orderedFreq', 'upperStateEnergyK',
        'thetaB', 'partFunc', 'inten', 'telescope')):
        """Generage XML doc for stick values"""

        #Create minidom document
        doc = Document()

        #Create tableData base element
        tableData = doc.createElement("TABLEDATA")
        doc.appendChild(tableData)

        for row in self.computedValues:
            #Create row element
            tr = doc.createElement("TR")
            tableData.appendChild(tr)

            #Dict key/value pairs for each row
            for key in keyOrder:
                elem = doc.createElement(key)
                tr.appendChild(elem)

                elemVal = doc.createTextNode(str(row[key]))
                elem.appendChild(elemVal)

        self.docXML = doc

        return None

    def writeXMLSticks(self, filename='', stdout=True, fileout=False):
        """Write out XML sticks file"""
        outString = self.docXML.toprettyxml(indent="    ")

        if (fileout == True):
            try:
                f = open(filename, 'w')
                f.write(outString)
                f.close()
            except IOError:
                sys.exit("Unable to open / write file")
        if (stdout == True):
            sys.stdout.write(outString)

        return None

    def openXMLInputs(self, filename='', fileinput=False, socket=True,
                      socketString=''):
        """Open XML input file or read stream from socket"""

        if (fileinput == True):
            try:
                doc = readConfig(filename=filename, fileinput=fileinput)
            except IOError:
                sys.exit("Unable to open / File does not exist")

        if (socket == True):
            try:
                doc = readConfig(socket=socket, socketString=socketString)
            except:
                sys.exit("Unable to parse incoming XML.")

        self.speciesIDNumber = int(doc['dataSplatInput']['speciesIDNumber'])
        self.temperature = float(doc['dataSplatInput']['temperature'])
        self.columnDensity = float(doc['dataSplatInput']['columnDensity'])
        self.lineWidth = float(doc['dataSplatInput']['lineWidth'])
        self.antennaDiameter = float(doc['dataSplatInput']['antennaDiameter'])
        self.sourceSize = float(doc['dataSplatInput']['sourceSize'])
        self.partFuncCoeff = float(doc['dataSplatInput']['partFuncCoeff'])
        self.partFuncExp = float(doc['dataSplatInput']['partFuncExp'])
        self.minFreq = float(doc['dataSplatInput']['minFreq'])
        self.maxFreq = float(doc['dataSplatInput']['maxFreq'])

        return None

    def openASCIIInputs(self, filename='', fileinput=False, socket=False,
                        socksetString=''):
        """Open simple ASCII text file or read stream from socket"""
        
        if (fileinput == True):
            try:
                params = [k for k in open(filename).read().split('\n') if k]
            except IOError:
                sys.exit("Unable to open / File does not exist")
        
        self.speciesIDNumber = int(params[0])
        self.temperature = float(params[1])
        self.columnDensity = float(params[2])
        self.lineWidth = float(params[3])
        self.antennaDiameter = float(params[4])
        self.sourceSize = float(params[4])
        self.partFuncCoeff = float(params[5])
        self.partFuncExp = float(params[6])
        self.minFreq = float(params[7])
        self.maxFreq = float(params[8])

        return None

    def plotSticks(self):
        """Make a matplotlib window of the data"""
        freqs = [item['orderedFreq'] for item in self.computedValues]
        intens = [item['inten'] for item in self.computedValues]
        pylab.vlines(freqs, 0, intens, 'b')
        pylab.xlabel(r'Frequency [GHz]')
        pylab.ylabel(r'Intensity')
        pylab.show()

        return True

    def printResults(self, keyOrder=('orderedFreq', 'upperStateEnergyK',
        'thetaB', 'partFunc', 'inten', 'telescope'), filename='',
        stdout=True, fileout=False):
        """Print formatted ASCII table of results to screen"""
        outString = ''
        outString += ''.join([s.center(12) for s in keyOrder]) + '\n'
        outString += '-------------------------------------------------\n'
        for row in self.computedValues:
            sf = "{0:.6f} {1:3.6f} {2:.6f} {3:.4f} {4:.4e} {5:s}"
            outString += (sf.format(row[keyOrder[0]],
                row[keyOrder[1]],
                row[keyOrder[2]],
                row[keyOrder[3]],
                row[keyOrder[4]],
                row[keyOrder[5]]) + '\n')

        if (fileout == True):
            try:
                f = open(filename, 'w')
                f.write(outString)
                f.close()
            except IOError:
                sys.exit("Unable to open / write file")
        if (stdout == True):
            sys.stdout.write(outString)

        return True

    def dataSplatSocket(self, host='', port=8081, bufsize=1024, silent=True):
        """The dataSplat module will listen for an XML file to process at
        a given host and port number"""

        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.bind((host, port))
        if (silent != True):
            sys.stdout.write("Listening on port:" + str(port) + "\n")
        while True:
            data, addr = s.recvfrom(bufsize)
            self.openXMLInputs(socket=True, socketString=data)
            dbpars = [k for k in open('splatdb.txt').read().split('\n') if k]
            self.splatSQLquery(dbpars[0], dbpars[1], dbpars[2], dbpars[3])
            self.computeVals()
            self.createXMLSticks()
            self.writeXMLSticks(stdout=True)
            self.printResults(stdout=True)

    def interpretArgs(self, args):
        """Interpret command line arguments parsed from the
           CommandLine class
        """
        if (args.demo == True):
            self.runDemo()
            return

        if (args.socket == True):
            host = ''
            port = 8081
            if (args.host):
                host = args.host
            if (args.port):
                port = args.port
            self.dataSplatSocket(host=host, port=port)
            return
            
        if (args.singleDish == True):
            singleDish = True
        
        if (args.singleDish == False):
            singleDish = False
        
        if (args.interferometer == True):
            interferometer = True
            
        if (args.interferometer == False):
            interferometer = False
        
        #Default to single dish if no parameter given...
        if (singleDish==False and interferometer == False):
            singleDish = True 
        
        if (args.inputxml and args.outputxml):
            self.openXMLInputs(filename=args.inputxml,
                fileinput=True, socket=False)
            dbpars = [k for k in open('splatdb.txt').read().split('\n') if k]
            self.splatSQLquery(dbpars[0], dbpars[1], dbpars[2], dbpars[3])
            self.computeVals(singleDish=singleDish, interferometer=interferometer)
            self.createXMLSticks()
            self.writeXMLSticks(filename=args.outputxml,
                stdout=False, fileout=True)
            
        if (args.inputxml and args.stdoutxml):
            self.openXMLInputs(filename=args.inputxml,
                fileinput=True, socket=False)
            dbpars = [k for k in open('splatdb.txt').read().split('\n') if k]
            self.splatSQLquery(dbpars[0], dbpars[1], dbpars[2], dbpars[3])
            self.computeVals(singleDish=singleDish, interferometer=interferometer)
            self.createXMLSticks()
            self.writeXMLSticks(filename=args.outputxml,
                stdout=True, fileout=False)
        
        if (args.inputxml and args.stdoutascii):
            self.openXMLInputs(filename=args.inputxml,
                fileinput=True, socket=False)
            dbpars = [k for k in open('splatdb.txt').read().split('\n') if k]
            self.splatSQLquery(dbpars[0], dbpars[1], dbpars[2], dbpars[3])
            self.computeVals(singleDish=singleDish, interferometer=interferometer)
            self.printResults(stdout=True, fileout=False)

    def runDemo(self):
        """Run demo of dataSplat"""
        self.setInputs(speciesIDNumber=77)
        #dbpars = [k for k in open('splatdb.txt').read().split('\n') if k]
        #self.splatSQLquery(dbpars[0], dbpars[1], dbpars[2], dbpars[3])
        self.splatSQLlitequery()
        self.computeVals(singleDish=True, interferometer=True)
        self.createXMLSticks()
        self.writeXMLSticks(filename='test.xml', stdout=True, fileout=False)
        self.printResults(filename='test.dat', stdout=True, fileout=False)
        self.plotSticks()

if __name__ == '__main__':

    if len(sys.argv) == 1:
        sys.argv.append('-h')

    c = commandline.CommandLine(sys)
    args = c.parser.parse_args()
    d = DataSplat()
    d.interpretArgs(args)
